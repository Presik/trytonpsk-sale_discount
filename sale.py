# This file is part of sale_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.modules.sale.sale import SaleReport
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
    'invisible': Eval('type') != 'line',
}

_ZERO = Decimal('0.0')


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    total_discount = fields.Function(fields.Numeric('Total Discount',
        digits=(16, 2)), 'get_total_discount')

    def get_total_discount(self, name=None):
        res = []
        for line in self.lines:
            if line.discount and line.discount != 0 and hasattr(line, 'unit_price_w_tax') and line.unit_price_w_tax and line.quantity:
                if line.discount != 1:
                    sale_price = line.unit_price_w_tax / (1 - line.discount)
                else:
                    sale_price = 0
                res.append(abs(sale_price - line.amount))

            # elif line.unit_price \
            #     and line.quantity and line.unit_price < line.product.list_price * Decimal(line.quantity):
            #
            #     res.append(line.product.list_price * Decimal(line.quantity) - line.unit_price)
        res = Decimal(sum(res)).quantize(Decimal(str(10.0 ** -2)))
        return res


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    discount = fields.Numeric('Discount', digits=(16, 6), states=STATES)

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        if 'discount' not in cls.product.on_change:
            cls.product.on_change.add('discount')
        if 'discount' not in cls.amount.on_change_with:
            cls.amount.on_change_with.add('discount')
        if 'discount' not in cls.quantity.on_change:
            cls.quantity.on_change.add('discount')
        if 'discount' not in cls.unit_price.depends:
            cls.unit_price.depends.append('discount')

    # def get_invoice_line(self):
    #     lines = super(SaleLine, self).get_invoice_line()
    #     pool = Pool()
    #     InvoiceLine = pool.get('account.invoice.line')
    #     if hasattr(InvoiceLine, 'discount'):
    #         for line in lines:
    #             if self.discount:
    #                 line.discount = float(round(self.discount, 2))
    #                 line.gross_unit_price = round((float(self.unit_price) / (float(self.discount) * 100)), 2)
    #                 line.unit_price = self.unit_price
    #     return lines

    def update_prices(self):
        # TODO: Add compute with context sale price list
        if self.discount and self.discount > _ZERO:
            list_price = self.product.list_price
            price = float(list_price) * (1 - float(self.discount))
            unit_price = Decimal(price).quantize(Decimal(str(10.0 ** -4)))
            self.unit_price = unit_price

    @fields.depends('product', 'discount', 'unit_price', 'sale')
    def on_change_discount(self):
        if not self.sale:
            self.sale = Transaction().context.get('sale')
        if self.discount and self.product:
            self.update_prices()

    @fields.depends('product', 'discount', 'unit_price', 'sale')
    def on_change_quantity(self):
        super(SaleLine, self).on_change_quantity()
        self.update_prices()

    @fields.depends('product', 'discount', 'unit_price', 'sale')
    def on_change_product(self):
        super(SaleLine, self).on_change_product()
        self.discount = None

    def get_invoice_line(self):
        invoice_lines = super(SaleLine, self).get_invoice_line()
        if self.discount and invoice_lines:
            invoice_lines[0].discount = Decimal(round(self.discount, 4))
        return invoice_lines


class SaleReport(SaleReport):
    __name__ = 'sale.sale.discount'
